# Dades del projecte de programació #

Grup autor : 

- Francesc Xavier Bullich
- Gil Gassó
- Marc Sánchez 

Aquest repositori està pensat per poder compartir entre tots els grups que ho requereixin totes les dades que es vulguin, la idea és que qualsevol persona que hagi creat les dades tingui la opció de tenir-ne més i fins i tot que pugui penjar les seves. 

Per tenir els jocs de proves preparats per provar les classes independentment es generaran diferents fitxers de dades i no un de sol. Qui vulgui pot afegir un fitxer general amb totes les dades peró preferiblement totes les que existeixin al fitxer general haurien d'estar en el seu propi fitxer de dades. 

El repositori és públic i tothom hi pot pujar coses. 

Animem a que tothom aporti!!